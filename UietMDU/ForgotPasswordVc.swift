//
//  ForgotPasswordVc.swift
//  UietMDU
//
//  Created by sachin yadav on 24/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit
import Firebase

class ForgotPasswordVc: UIViewController {
    
    
    @IBOutlet weak var resetbtn: UIButton!
    
    @IBOutlet weak var emailfield: UITextField!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        resetbtn.layer.borderColor = clr.cgColor
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dissmiskeyboard))
        self.view.addGestureRecognizer(gesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func forgotPassword(_ sender: UIButton) {
        resetbtn.isEnabled = false
        
        if self.emailfield.text == "" || self.emailfield.text == nil
        {
            let alertController = UIAlertController(title: "Oops!", message: "Please enter  email.", preferredStyle: .alert)
            
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            FIRAuth.auth()?.sendPasswordReset(withEmail: self.emailfield.text!, completion: { (error) in
                
                var title = ""
                var message = ""
                
                if error != nil
                {
                    title = "Oops!"
                    message = (error?.localizedDescription)!
                }
                else
                {
                    title = "Success!"
                    message = "Password reset email sent."
                    self.emailfield.text = ""
                }
                
                let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
                self.resetbtn.isEnabled = true
            })
        }
    

    
    }
    func dissmiskeyboard() {
        
        self.view.endEditing(true)
    }


    @IBAction func go(sender:UIButton) {
    
    self.dismiss(animated: true, completion: nil)
    
        
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
