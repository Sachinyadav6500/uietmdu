//
//  HomeVc.swift
//  UietMDU
//
//  Created by sachin yadav on 15/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class HomeVc: UIViewController {
    
    @IBOutlet var Menubtn:UIBarButtonItem!
    @IBOutlet var topheight: [NSLayoutConstraint]!
    @IBOutlet var leftright: [NSLayoutConstraint]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
// need modification
        let height = UIScreen.main.bounds.height
        
        switch height {
        case 667.0:
            for x in topheight
            {
                x.constant = 40.0
                
            }
            for x in leftright
            {
                x.constant = 20.0
            }
            break
        case 568.0 :
            for x in topheight
            {
                x.constant = 30.0
                
            }
            for x in leftright
            {
                x.constant = 12.0
            }
            break
        case 736:
            for x in topheight
            {
                x.constant = 50.0
                
            }
            for x in leftright
            {
                x.constant = 20.0
                
            }
            break
        default: break
            //code
        }
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        Menubtn.target = self.revealViewController()
        Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
        

        
        print("this is height")
        print(height)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func setting(sender:UIButton){
        tag = 1
        
        switch sender.tag {
        case 1:
            
            let vc = storyboard?.instantiateViewController(withIdentifier: "notices")
            self.present(vc!, animated: true, completion: nil)
            break
        case 2:
            let vc = storyboard?.instantiateViewController(withIdentifier: "placements")
            self.present(vc!, animated: true, completion: nil)

            break

        case 3:
            let vc = storyboard?.instantiateViewController(withIdentifier: "profile")
            self.present(vc!, animated: true, completion: nil)
            
            break
        case 4:
            let vc = storyboard?.instantiateViewController(withIdentifier: "syllabus")
            self.present(vc!, animated: true, completion: nil)

            break

        case 5:
            let vc = storyboard?.instantiateViewController(withIdentifier: "timetable")
            self.present(vc!, animated: true, completion: nil)

            break

        case 6:
            let vc = storyboard?.instantiateViewController(withIdentifier: "facultylist")
            self.present(vc!, animated: true, completion: nil)

            break

        case 7:
            let vc = storyboard?.instantiateViewController(withIdentifier: "settings")
            self.present(vc!, animated: true, completion: nil)

            break

        case 8:
            let vc = storyboard?.instantiateViewController(withIdentifier: "departments")
            self.present(vc!, animated: true, completion: nil)

            break

        case 9:
            let vc = storyboard?.instantiateViewController(withIdentifier: "about")
            self.present(vc!, animated: true, completion: nil)

            break

        

        default:
            break
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let default1 = UserDefaults.standard
        let data = default1.data(forKey: "User_image")
        let name  = default1.string(forKey: "User_name")
        let email  = default1.string(forKey: "User_email")
        let register  = default1.string(forKey: "User_registration")
        let rool  = default1.string(forKey: "User_rollno")
        
        appuser.name = name!
        appuser.email = email!
        appuser.registration = register!
        appuser.Rollno = rool!
        appuser.img = UIImage(data: data!)!
        print(name)
        print(appuser.name)
        

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
