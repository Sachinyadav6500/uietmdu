//
//  MenuRearview.swift
//  UietMDU
//
//  Created by sachin yadav on 29/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit
import Firebase

class MenuRearview: UIViewController {
    
    @IBOutlet weak var mainimage: UIButton!
    
    @IBOutlet weak var name: UILabel!
    var networkstatus:String? = nil
    
    @IBOutlet weak var logoutbtn: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let clr2 = UIColor(colorLiteralRed: 0.56640625, green: 0.0, blue: 0.1484375, alpha: 1.0)
                   logoutbtn.layer.borderColor = UIColor.white.cgColor
                     line()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        mainimage.setImage(appuser.img, for: .normal)
        name.text = appuser.name
    }
    
    func line() {
        let height = UIScreen.main.bounds.height - 44.0
        
        let height2 =  height/6
        let arr = [1,2,3,3.9,4.9]
        for i in arr
{
            let y = 44.0 + height2 * CGFloat(i)
            let view1 = UIView()
            view1.frame = CGRect(x: 0.0, y: y , width: 100, height: 1)
            view1.backgroundColor = UIColor.white
            self.view.addSubview(view1)
        }
    }
    
    
    @IBAction func logout(sender:UIButton){
        try!   FIRAuth.auth()?.signOut()

        self.dismiss(animated: true) {
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
