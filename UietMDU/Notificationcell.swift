//
//  Notificationcell.swift
//  UietMDU
//
//  Created by sachin yadav on 01/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class Notificationcell: UITableViewCell {
    
    @IBOutlet var switch1:UISwitch!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func notification(sender:UISwitch){
        
        if sender.isOn == true{
            print("it's on" )
        }
        else {
            print("it's off")
        }
        
    }
    
    
}
