//
//  RegisterVC.swift
//  UietMDU
//
//  Created by sachin yadav on 24/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit
import Firebase

class RegisterVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var regbtn: UIButton!
    @IBOutlet weak var image: UIButton!
    var image1:UIImage! = nil
    @IBOutlet weak var email:UITextField!
    @IBOutlet weak var password:UITextField!
    @IBOutlet weak var confirmpass:UITextField!
    @IBOutlet weak var name:UITextField!
    @IBOutlet weak var rollno:UITextField!
    @IBOutlet weak var registrationNo:UITextField!

    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        regbtn.layer.borderColor = clr.cgColor
        //keyboard ending
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dissmiskeyboard))
        self.view.addGestureRecognizer(gesture)

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if image1 != nil {
        self.image.setImage(self.image1, for: .normal)
    }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func signup(sender:UIButton){
        
        if image1 == nil || email.text == "" || email.text == nil || password.text == "" || password.text == nil || confirmpass.text == "" || confirmpass.text == nil || name.text == "" || name.text == nil  || rollno.text == "" || rollno.text == nil ||  registrationNo.text == "" || registrationNo.text == nil {
            
            let alert = UIAlertController(title: "Oops!", message: "you havn't fill all details or havn't pick image", preferredStyle: .alert)
            let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
            alert.addAction(action)
            self.present(alert, animated: true, completion: nil)
            
            
        }else
        {
            if password.text != confirmpass.text {
                
                let alert = UIAlertController(title: "Oops!", message: "Password don't match", preferredStyle: .alert)
                let action = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
                

            }else
            {
                
                FIRAuth.auth()?.createUser(withEmail: email.text!, password: password.text!, completion: {
                    user, error in
                    
                    if error != nil{
                        if let message:String = (error?.localizedDescription)
                        {
                            let alert = UIAlertController(title: "Not Registration ", message: message, preferredStyle: UIAlertControllerStyle.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            print("Email has been used, try a different one")
                        }
                    }
                        
                    else{
                        
                        FIRAuth.auth()?.currentUser!.sendEmailVerification(completion: { (error) in
                            
                            if error == nil
                            {
                                let alert = UIAlertController(title: "Account Created", message: "Please verify your email by confirming the sent link.", preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "ok", style: .default, handler: { (action:UIAlertAction) in
                                    
                                    let data:NSData = UIImagePNGRepresentation(self.image1) as! NSData
                                    let defaults = UserDefaults.standard
                                    let user1:User = User(name: self.name.text!, email: self.email.text!, password: self.password.text!, img: self.image1, Rollno: self.rollno.text!, registration: self.registrationNo.text!)
                                    
                                    
                                    defaults.set(user1.email, forKey: "User_email")
                                    defaults.set(user1.name, forKey: "User_name")
                                    defaults.set(user1.Rollno, forKey: "User_rollno")
                                    defaults.set(user1.registration, forKey: "User_registration")
                                    defaults.set(data, forKey: "User_image")


                                
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }))
                                self.present(alert, animated: true, completion: nil)
                                
                            }
                            else{
                                let meesage:String = (error?.localizedDescription)!
                                let alert = UIAlertController(title: "try later", message: meesage, preferredStyle: UIAlertControllerStyle.alert)
                                
                                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                                
                                
                                
                            }
                        })
                    }
                })

            }
        }

    }
    
    @IBAction func logInVc(_ sender: UIButton) {
        
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func dissmiskeyboard() {
        
        self.view.endEditing(true)
    }
    
    
    @IBAction func changeimage(sender:UIButton)
        
    {
        let photocontroler = UIImagePickerController()
        photocontroler.delegate = self
        photocontroler.sourceType = .photoLibrary
        self.present(photocontroler, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        image1 = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.dismiss(animated: true, completion: nil)
    }
   

    /*
    // MARK: - Navigationl

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
