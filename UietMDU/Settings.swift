//
//  Settings.swift
//  UietMDU
//
//  Created by sachin yadav on 31/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class Settings: UIViewController ,UITableViewDataSource,UITableViewDelegate{

    
    @IBOutlet var switch1:UISwitch!
    @IBOutlet var Menubtn:UIBarButtonItem!
    
    let section = ["General Settings ", "More "]

    override func viewDidLoad() {
        super.viewDidLoad()
        Menubtn.tag = tag

        if Menubtn.tag == 0 {
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        Menubtn.target = self.revealViewController()
        Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
        
        else{
            Menubtn.target = self
            Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
            Menubtn.action = #selector(self.dissmiss)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return self.section[section]
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 52.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else{
            return 3
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "boom"+"\(indexPath.section)" + "\(indexPath.row)", for: indexPath)
        return cell
        
   
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("u select the selector")
    }
    
    
    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
