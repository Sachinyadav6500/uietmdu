//
//  UserProfile.swift
//  UietMDU
//
//  Created by sachin yadav on 26/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class UserProfile: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    
    @IBOutlet weak var btnimg: UIButton!
    @IBOutlet var Menubtn:UIBarButtonItem!
    @IBOutlet var img:UIImageView!
    var image1:UIImage! = nil
    
    @IBOutlet weak var changeProfilebtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeProfilebtn.layer.borderColor = clr.cgColor
       img.addBlurEffect()
        
        Menubtn.tag = tag
        
        if Menubtn.tag == 0 {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            Menubtn.target = self.revealViewController()
            Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
            
        else{
            Menubtn.target = self
            Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
            Menubtn.action = #selector(self.dissmiss)
        }

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if image1 != nil
        {
        btnimg.setImage(image1, for: .normal)
        img.image = image1
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeimage(sender:UIButton)
    {
        let photocontroler = UIImagePickerController()
        photocontroler.delegate = self
        photocontroler.sourceType = .photoLibrary
        self.present(photocontroler, animated: true, completion: nil)

        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        image1 = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.dismiss(animated: true, completion: nil)
    }
    
    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
