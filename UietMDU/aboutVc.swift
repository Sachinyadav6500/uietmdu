//
//  aboutVc.swift
//  UietMDU
//
//  Created by sachin yadav on 15/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class aboutVc: UIViewController,MKMapViewDelegate {

    @IBOutlet weak var map: MKMapView!

    
    let cord = CLLocationCoordinate2D(latitude: 28.8763831, longitude: 76.615276999)
    
    let annotation = CustomPointAnnotation()
    override func viewDidLoad() {
        super.viewDidLoad()
      map.delegate = self
        
        
    annotation.title = "UIET-MDU"
        annotation.coordinate = cord
        annotation.subtitle = "University Insitute of technology"
        annotation.imageName = "department_welcome"
        var cordspan = MKCoordinateSpan()
        cordspan.latitudeDelta = 0.04
        cordspan.longitudeDelta = 0.04
        let reagion = MKCoordinateRegion(center: cord, span: cordspan)
        map.setRegion(reagion, animated: true)
        map.addAnnotation(annotation)
        
    
    }
    
    func mapView(_ mapView: MKMapView!, viewFor annotation: MKAnnotation!) -> MKAnnotationView! {
        
        
        if !(annotation is CustomPointAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView?.canShowCallout = true
        }
        else {
            anView?.annotation = annotation
        }
        
        //Set annotation-specific properties **AFTER**
        //the view is dequeued or created...
        
        let cpa = annotation as! CustomPointAnnotation
        anView?.image = UIImage(named:cpa.imageName)
        anView?.frame = CGRect(x: annotation.coordinate.latitude, y: annotation.coordinate.longitude, width: 25.0, height: 35.0)
        
        return anView
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func back(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func safari(_sender: UIButton!)
    {
        print("haiiiii")
        if let url = NSURL(string: "http://www.uietmdu.com") {
        UIApplication.shared.openURL(url as URL)
        }    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
class CustomPointAnnotation: MKPointAnnotation {
    var imageName: String!
}
