//
//  facultyInfo.swift
//  UietMDU
//
//  Created by sachin yadav on 26/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class facultyInfo: UIViewController {

    @IBOutlet var Menubtn:UIBarButtonItem!
    
    
    @IBOutlet weak var savcont: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        savcont.layer.borderColor = clr.cgColor
        Menubtn.tag = tag
        
        if Menubtn.tag == 0 {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            Menubtn.target = self.revealViewController()
            Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
            
        else{
            Menubtn.target = self
            Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
            Menubtn.action = #selector(self.dissmiss)
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }


}
