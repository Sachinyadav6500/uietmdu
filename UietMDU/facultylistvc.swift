//
//  facultylistvc.swift
//  UietMDU
//
//  Created by sachin yadav on 13/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class facultylistvc: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var Menubtn:UIBarButtonItem!

    var arr = [teacher]()
    @IBOutlet var Table:UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        arr.append(teacher(name: "Sachin", email: "sachin551@live.com", rank: "boss", img: UIImage(named: "circle.png")!))
        arr.append(teacher(name: "Sachin", email: "sachin551@live.com", rank: "boss", img: UIImage(named: "circle.png")!))
        arr.append(teacher(name: "Sachin", email: "sachin551@live.com", rank: "boss", img: UIImage(named: "circle.png")!))
        arr.append(teacher(name: "Sachin", email: "sachin551@live.com", rank: "boss", img: UIImage(named: "circle.png")!))
        
        Menubtn.tag = tag
        
        if Menubtn.tag == 0 {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            Menubtn.target = self.revealViewController()
            Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
            
        else{
            Menubtn.target = self
            Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
            Menubtn.action = #selector(self.dissmiss)
        }

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return arr.count
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! facultylistTableViewCell
       let  data = arr[indexPath.row]
        cell.email.text = data.email
        cell.name.text = data.name
        cell.rank.text = data.rank
        cell.teacherimg.image = data.img

        // Configure the cell...

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }
    

}
