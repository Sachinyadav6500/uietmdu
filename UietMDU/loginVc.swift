//
//  ViewController.swift
//  UietMDU
//
//  Created by sachin yadav on 20/10/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit
import Firebase

class loginVc: UIViewController {

    @IBOutlet weak var loginbtn: UIButton!
    @IBOutlet weak var text1:UITextField!
    @IBOutlet weak var text2:UITextField!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginbtn.layer.borderColor = clr.cgColor
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.dissmiskeyboard))
        //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())

        self.view.addGestureRecognizer(gesture)

        // Do any additional setup after loading the view, typically from a nib.
    }

    
    @IBAction func loginVC(_ sender: UIButton) {
        
        FIRAuth.auth()?.signIn(withEmail: self.text1.text!, password: self.text2.text!) {
            (user, error) in
            
            if error == nil {
                if let user = FIRAuth.auth()?.currentUser {
                    if !user.isEmailVerified{
                        
                        let alertVC = UIAlertController(title: "Error", message: "Sorry. Your email address has not yet been verified. Do you want us to send another verification email to \(self.text1.text!).", preferredStyle: .alert)
                        let alertActionOkay = UIAlertAction(title: "Okay", style: .default) {
                            (_) in
                            user.sendEmailVerification(completion: nil)
                        }
                        let alertActionCancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                        
                        alertVC.addAction(alertActionOkay)
                        alertVC.addAction(alertActionCancel)
                        self.present(alertVC, animated: true, completion: nil)
                    }
                    else {
                        print ("Email verified. Signing in...")
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "rhome")
                        self.present(vc!, animated: true, completion: nil)
                        
                    }
                }
            }
            else{
                let message:String = (error?.localizedDescription)!
                let alertvc = UIAlertController(title: "Error in Login", message: message, preferredStyle: .alert)
                alertvc.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alertvc, animated: true, completion: nil)
             
            }
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func dissmiskeyboard() {
        
        self.view.endEditing(true)
    }

    
}

