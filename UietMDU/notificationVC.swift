//
//  notificationVC.swift
//  
//
//  Created by sachin yadav on 15/11/16.
//
//

import UIKit

class notificationVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
        var arr = [Notices]()
    var disc = [String:String]()

    
       // let clr = UIColor(displayP3Red: 0.451, green: 0.008, blue: 0.039, alpha: 1.0)
        
        @IBOutlet var table:UITableView!
        @IBOutlet var Menubtn:UIBarButtonItem!
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            newsANdoffer()
            Menubtn.tag = tag
            
            if Menubtn.tag == 0 {
                
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                Menubtn.target = self.revealViewController()
                Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
                
            }
                
            else{
                Menubtn.target = self
                Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
                Menubtn.action = #selector(self.dissmiss)
            }
            

        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        // MARK: - Table view data source
        
        func numberOfSections(in tableView: UITableView) -> Int {
            // #warning Incomplete implementation, return the number of sections
            return 1
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            // #warning Incomplete implementation, return the number of rows
            return arr.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! notificationTableViewCell
            
            if arr.count >= 0 {
                let notice = arr[indexPath.row]
             
                    cell.tittle.text = notice.tittle
                    cell.content.text = notice.content
                     cell.time.text = notice.time
                
                    
                
            }
            return cell
        }
    
    
    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }
    

    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
        func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return 0.0
        }
        func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            return 0.0
        }
        

    func newsANdoffer()
    {
        
        DispatchQueue.global().async {

            let getUserStr =  "http://pup.maibiz.in/api/mobileroll/getoffer/mobileapi"
            
            
            let request = NSMutableURLRequest(url: NSURL(string: getUserStr) as! URL)
            let session = URLSession.shared
            request.httpMethod = "GET"
            
            
            let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
                
                
                // Did the JSONObjectWithData constructor return an error? If so, log the error to the console
                if(error != nil) {
                    print(error?.localizedDescription)
                }
                else {
                    
                    print(response)
                    print("data\(data)")
                    //let strData = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue))
                    // print("Body: \(strData)")
                    do{
                        
                        let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as?
                        NSArray
                        for  x  in json!
                        {
                            
                            let str = x as! NSDictionary
                            
                            
                            
                            let date1 = str["created_on"] as! String
                            print(date1)
                            let datear = date1.components(separatedBy: " ")
                            let date = datear[0]

                            let tittle = str["title"] as! String
                            let desc = str["description"] as! String
                            let note = Notices(tittle: tittle, content: desc, time: date)
                            
                            self.arr.append(note)
                            
                            
                            
                        }
                        DispatchQueue.main.async {
                            self.table.reloadData()

                        }
                        
                    }
                    catch
                    {
                        print("error in output data")
                    }
                    
                }
            })
            
            task.resume()
            
            
        }

        
        }
            
        }
    

