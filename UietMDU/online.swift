//
//  online.swift
//  UietMDU
//
//  Created by sachin yadav on 11/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class online: UILabel {

    
    var internet:Bool? = nil
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        
        DispatchQueue.global().async {
            self.internet = isConnectedToNetwork()
            if self.internet == true
            {
                DispatchQueue.main.async {
                    self.textColor = .green
                    self.text = "online"
                }
            }else
            {
                self.textColor = .red
                self.text = "offline"
                
            }
        }

        
    }
}
