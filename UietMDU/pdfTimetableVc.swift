//
//  pdfTimetableVc.swift
//  UietMDU
//
//  Created by sachin yadav on 15/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class pdfTimetableVc: UIViewController {

    @IBOutlet var Menubtn:UIBarButtonItem!
    @IBOutlet var web:UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        Menubtn.tag = tag
        
        if Menubtn.tag == 0 {
            
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            Menubtn.target = self.revealViewController()
            Menubtn.action = #selector(SWRevealViewController.revealToggle(_:))
            
        }
            
        else{
            Menubtn.target = self
            Menubtn.setBackButtonBackgroundImage(UIImage(named :"back.png"), for: .normal, barMetrics: .default)
            Menubtn.action = #selector(self.dissmiss)
        }
/// need changes
        

        let targetURL = Bundle.main.url(forResource: "BtechCse3rdSem(A)", withExtension: "pdf")!
        
        // This value is force-unwrapped for the sake of a compact example, do not do this in your code
        let request = NSURLRequest(url: targetURL)
        web.loadRequest(request as URLRequest)
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dissmiss(){
        tag = 0
        self.dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
