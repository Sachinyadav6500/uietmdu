//
//  syllebuspdfVc.swift
//  UietMDU
//
//  Created by sachin yadav on 15/11/16.
//  Copyright © 2016 sachin yadav. All rights reserved.
//

import UIKit

class syllebuspdfVc: UIViewController {

    @IBAction func menu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet var web:UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let targetURL = Bundle.main.url(forResource: "Syl_CSE_2", withExtension: "pdf")!
        
        // This value is force-unwrapped for the sake of a compact example, do not do this in your code
        let request = NSURLRequest(url: targetURL)
        web.loadRequest(request as URLRequest)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue:  UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
